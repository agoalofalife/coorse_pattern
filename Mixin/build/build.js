var observer =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	var _mixin = __webpack_require__(1);

	var Mixin_Babbler = {
	    say: function say() {
	        console.log("My name is " + this.name + " and i think:'" + this.THOUGHTS + "'");
	    },
	    argue: function argue() {
	        console.log("You're totally wrong");
	    }
	};

	var Mixin_BeverageLover = {
	    drink: function drink() {
	        console.log("* drinking " + this.FAVORITE_BEVERAGE + " *");
	    }
	};
	function Man(name) {
	    this.name = name;
	}
	Man.prototype = {
	    constructor: Man,
	    FAVORITE_BEVERAGE: 'Клинское',
	    THOUGHTS: "I like soccer"
	};

	(0, _mixin.mixin)(Man, Mixin_Babbler, Mixin_BeverageLover);
	var man = new Man("Bob");
	man.say();
	man.drink();

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.mixin = mixin;
	function mixin(object) {
	    var mixins = Array.prototype.slice.call(arguments, 1);

	    for (var i = 0; i < mixins.length; ++i) {
	        for (var prop in mixins[i]) {
	            if (typeof object.prototype[prop] === "undefined") {
	                object.prototype[prop] = mixins[i][prop];
	            }
	        }
	    }
	}

/***/ }
/******/ ]);