export function mixin(object) {
    let mixins = Array.prototype.slice.call(arguments, 1);

    for (let i = 0; i < mixins.length; ++i)
    {
        for (let prop in mixins[i])
        {
            if (typeof object.prototype[prop] === "undefined")
            {
                object.prototype[prop] = mixins[i][prop];
            }
        }
    }
}