import {mixin} from './mixin.js';

let Mixin_Babbler =
{
    say: function () { console.log("My name is " + this.name + " and i think:'" + this.THOUGHTS + "'"); },
    argue: function() { console.log("You're totally wrong"); }
};

var Mixin_BeverageLover =
{
    drink: function () { console.log("* drinking " + this.FAVORITE_BEVERAGE + " *"); }
};
function Man(name)
{
    this.name = name;
}
Man.prototype =
{
    constructor: Man,
    FAVORITE_BEVERAGE : 'Клинское',
    THOUGHTS: "I like soccer"
};

mixin(Man, Mixin_Babbler,Mixin_BeverageLover);
let man = new Man("Bob");
man.say();
man.drink();
