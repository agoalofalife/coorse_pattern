//Производитель
var Apple = {
    buy: function (model) {
        return this._create(model);
    },
    _create: function (model) {
        //сложный процесс производства модели
        return model;
    }
};

//Склад
var Stock = {
    _stock: [],
    //добавление на склад
    add: function (product) {
        this._stock.push(product);
    },
    //получение со склада
    get: function (product) {
        var self = this,
            stockProduct;


        this._stock.forEach(function (item, index) {
            if (item === product) {
                stockProduct = self._stock.splice(index, 1);
            }
        });

        return stockProduct ? stockProduct[0] : null;
    }
};
//Магазин
var Shop = {
    //фасад магазина - интерфейс для покупки
    buy: function (product) {
        return this._getFromStock(product);
    },
    //внутрений метод магазина по получению продукта
    _getFromStock: function (product) {
        var stockProduct;


        stockProduct = Stock.get(product);
        //если есть на складе - отдаем
        if (stockProduct) {
            return stockProduct;
        }
        //если нет на складе - покупаем у производителя и кладем на склад
        Stock.add(Apple.buy(product));

        //отдаем со склада
        return Stock.get(product);
    }
};

//Добавим на склад несколько моделей
Stock.add('iPhone 3g');
Stock.add('iPhone 4');
Stock.add('iPhone 4s');
Stock.add('iPhone 5s');

//Купим модель, которая есть на складе
console.log(Shop.buy('iPhone 3g'));
//Купим модель, которой нет на складе
console.log(Shop.buy('iPhone 6'));