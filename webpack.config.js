module.exports = {
    entry  : {
        observer: "./Observer/index.js",
    },
    output : {
        filename : './Observer/build/build.js',
        library  : '[name]'
    },
    watch : true,
    watchOptions : {
        aggregateTimeout : 100
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};
module.exports = {
    entry  : {
        factory : './Factory/index.js',
    },
    output : {
        filename : './Factory/build/build.js',
        library  : '[name]'
    },
    watch : true,
    watchOptions : {
        aggregateTimeout : 100
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};
module.exports = {
    entry  : {
        observer: "./Mixin/index.js",
    },
    output : {
        filename : './Mixin/build/build.js',
        library  : '[name]'
    },
    watch : true,
    watchOptions : {
        aggregateTimeout : 100
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};