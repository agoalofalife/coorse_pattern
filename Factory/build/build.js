var factory =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _Factory = __webpack_require__(1);

	var example = document.querySelector('#example');
	var dom = _Factory.taskManager.factory('getDom', example);
	console.log(dom.attr('name')); //получить аттрибут
	console.log(dom.text());
	console.log(dom.text('ПРивет'));

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var taskManager = {
	    read: function read() {
	        console.log('read');
	    },
	    getDom: function getDom(node) {
	        this.node = node;
	        this.attr = function (attribute) {
	            if (arguments.length <= 1) {
	                return this.node.getAttribute(attribute);
	            }
	        }, this.text = function () {
	            console.log(arguments.length);
	            return arguments.length === 0 ? this.node.innerText : this.node.innerText = arguments[0];
	        };
	    },
	    factory: function factory(typeType, parameters) {
	        return new taskManager[typeType](parameters);
	    }
	};
	exports.taskManager = taskManager;

/***/ }
/******/ ]);