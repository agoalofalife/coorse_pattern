let taskManager = {
    read : function () {
    console.log('read');
  },
    getDom : function (node) {
        this.node = node;
         this.attr = function (attribute) {
            if (arguments.length <= 1) {
                return this.node.getAttribute(attribute);
            }
        },
             this.text = function () {
                 console.log( arguments.length );
                return  arguments.length === 0 ? this.node.innerText : this.node.innerText = arguments[0];
        }
    },
    factory : function (typeType,parameters) {
    return new taskManager[typeType](parameters);
}
};
export {taskManager}