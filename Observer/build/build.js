var observer =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _IObserver = __webpack_require__(1);

	var app = document.querySelector('#app'),
	    AllTag = document.querySelectorAll('*[data-observer]');

	var blogger = {
	    superPush: function superPush(eventText) {
	        for (var i = 0; i < AllTag.length; i++) {
	            blogger.addSubscriber(function (newText, node) {
	                node.innerText = newText;
	            });
	            this.publish(eventText, AllTag[i]);
	        }
	    }
	};
	_IObserver.observer.make(blogger);

	app.addEventListener('input', function (e) {
	    blogger.superPush(e.target.value);
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var observer = {
	    //Добавить подписчика
	    addSubscriber: function addSubscriber(callback) {
	        this.subscribers[this.subscribers.length] = callback;
	    },
	    removeSubscriber: function removeSubscriber(callback) {
	        for (var i = 0; i < this.subscribers.length; i++) {
	            if (this.subscribers[i] === callback) {
	                delete this.subscribers[i];
	            }
	        }
	    },
	    //Разослать всем подписчикам
	    publish: function publish(what, where) {
	        for (var i = 0; i < this.subscribers.length; i++) {
	            if (typeof this.subscribers[i] === 'function') {
	                this.subscribers[i](what, where);
	            }
	        }
	    },
	    //Создать наблюдателя
	    make: function make(o) {
	        //превращает обьект в издателя
	        for (var i in this) {
	            o[i] = this[i];
	            o.subscribers = [];
	        }
	    }
	};

	exports.observer = observer;

/***/ }
/******/ ]);