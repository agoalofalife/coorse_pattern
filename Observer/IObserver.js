var observer = {
    //Добавить подписчика
    addSubscriber:function (callback) {
        this.subscribers[this.subscribers.length] = callback;
    },
    removeSubscriber:function (callback) {
        for (var i = 0; i < this.subscribers.length; i++) {
            if (this.subscribers[i] === callback) {
                delete(this.subscribers[i]);
            }
        }
    },
    //Разослать всем подписчикам
    publish:function (what,where) {
        for (var i = 0; i < this.subscribers.length; i++) {
            if (typeof this.subscribers[i] === 'function') {
                this.subscribers[i](what,where);
            }
        }
    },
    //Создать наблюдателя
    make:function (o) { //превращает обьект в издателя
        for (var i in this) {
            o[i] = this[i];
            o.subscribers = [];

        }
    }
};

export {observer}