import {observer} from './IObserver.js';

let app     = document.querySelector('#app'),
    AllTag  = document.querySelectorAll('*[data-observer]');

var blogger = {
    superPush:function (eventText) {
        for (var i = 0; i < AllTag.length; i++) {
            blogger.addSubscriber(function (newText,node) {
                node.innerText = newText;
            });
            this.publish(eventText,AllTag[i]);
        }
    }
};
observer.make(blogger);

app.addEventListener('input',e => {
    blogger.superPush(e.target.value)
});